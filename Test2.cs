internal class Program
{
    public static string Compress(string input)
    {
        char[] charArr = input.ToCharArray();
        string result = "";
        int count = 1;
        for (int i = 0; i < charArr.Length; i++)
        {
            int j = i + 1;
            while (j < charArr.Length && charArr[i] == charArr[j])
            {
                j++;
                count++;
            }
            result += count + "" + charArr[i];
            count = 1;
            i = j - 1;
        }
        return result;
    }

    public static string Decompress(string input)
    {
        char[] charArr = input.ToCharArray();
        string result = "";
        for (int i = 0; i < charArr.Length; i++)
        {
            int j = i + 1;
            if (j < charArr.Length)
            {
                int amount = Convert.ToInt32(charArr[i].ToString());
                for (int k = 0; k < amount; k++)
                {
                    result += charArr[j];
                }
            }
            i = j;
        }
        return result;
    }

    public static void Main(string[] args)
    {
        string result = Compress("HELLOOO");
        Console.WriteLine("Compress Result : " + result);

        string result2 = Decompress(result);
        Console.WriteLine("Decompress Result : " + result2);
    }
}
