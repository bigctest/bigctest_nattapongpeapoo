internal class Program
{
    private static List<List<int>> Generate(List<List<int>> inputArray)
    {
        List<List<int>> generateResult =
            inputArray
                .Take(1)
                .FirstOrDefault()
                .Select(i => (new int[] { i }).ToList())
                .ToList();

        foreach (List<int> series in inputArray.Skip(1))
        {
            generateResult =
                generateResult
                    .Join(series as List<int>,
                    combination => true,
                    i => true,
                    (combination, i) =>
                    {
                        List<int> nextLevel = new List<int>(combination);
                        nextLevel.Add (i);
                        return nextLevel;
                    })
                    .ToList();
        }

        return generateResult;
    }

    public static List<List<int>> Combination(List<List<int>> input)
    {
        List<List<int>> result = Generate(input);

        return result;
    }

    public static void Main(string[] args)
    {
        // Test Data
        var array = new List<List<int>>();
        var arr1 = new List<int>() { 1 };
        var arr2 = new List<int>() { 2 };
        var arr3 = new List<int>() { 3, 4 };
        var arr4 = new List<int>() { 5, 6 };
        var arr5 = new List<int>() { 7, 8 };

        array.Add (arr1);
        array.Add (arr2);
        array.Add (arr3);
        array.Add (arr4);
        array.Add (arr5);

        var result = Combination(array);

        Console.ReadKey();
    }
}
