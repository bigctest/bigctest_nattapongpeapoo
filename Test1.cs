internal class Program
{
    public static string mysolution(string input)
    {
        char[] charArr = input.ToCharArray();
        string result = "";

        foreach (char c in charArr)
        {
            int countDuplicate = charArr.Where(x => x == c).Count();
            if (countDuplicate == 1)
            {
                result = c.ToString();
                break;
            }
        }

        if (string.IsNullOrEmpty(result))
        {
            result = "_";
        }
        return result;
    }

    public static void Main(string[] args)
    {
        string result = mysolution("abacabadabacaba");
        Console.WriteLine("Result : " + result);
    }
}
