package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/jszwec/csvutil"
)

const (
	success string = "success"
	fail    string = "fail"
)

type csvContent struct {
	ID   int
	Name string
	Age  int
	Team string
}
type resData struct {
	Total  int          `json:"total"`
	Record []csvContent `json:"record"`
}

type response struct {
	Code string  `json:"code"`
	Data resData `json:"data"`
}

type responseError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func getFileByName(id string) ([]csvContent, error) {
	// Check file existing
	filePath := "files\\" + id + ".csv"
	fmt.Println("Path " + filePath)
	if _, err := os.Stat(filePath); err == nil {
		fmt.Println("Exist " + filePath)
		csv_file, _ := os.Open(filePath)
		reader := csv.NewReader(csv_file)
		reader.Comma = ','

		_, err := reader.Read() // skip first line
		if err != nil {
			if err != io.EOF {
				log.Fatalln(err)
			}
		}

		userHeader, _ := csvutil.Header(csvContent{}, "csv")
		dec, _ := csvutil.NewDecoder(reader, userHeader...)
		var csvContents []csvContent
		for {
			var item csvContent
			// Map to struct
			if err := dec.Decode(&item); err == io.EOF {
				break
			}
			csvContents = append(csvContents, item)
		}
		fmt.Println(csvContents)
		return csvContents, nil
	}

	return nil, errors.New("file not found")
}

func getFile(context *gin.Context) {
	id := context.Param("id")
	csvContents, err := getFileByName(id)

	if err != nil {
		var resError responseError
		resError.Code = fail
		resError.Message = "file not found"
		context.IndentedJSON(http.StatusNotFound, resError)
		return
	}
	var resData response
	resData.Code = success
	resData.Data.Record = csvContents
	resData.Data.Total = len(csvContents)

	context.IndentedJSON(http.StatusOK, resData)
}

func main() {
	router := gin.Default()
	router.GET("/file/:id", getFile)

	// Server Listening
	router.Run("localhost:5000")
}
